var irc = require("irc");
var parser = require('./lib/parser');

var settings = {
    channels : ["#semicolonoscopy", "#twitchplayssalem"],
    server : "irc.twitch.tv",
    port: 6667,
    secure: false,
    nick : "twitchplayssalem",
    password : "oauth:7o0y2w480dhifqf2cy281aqvj0gd5m" 
};

var ircContainer = {
	messagePool: [],
	bot: null,
	start: function(){

		if (ircContainer) delete ircContainer.bot;

		ircContainer.bot = new irc.Client(settings.server, settings.nick, {
		    channels: [settings.channels + " " + settings.password],
		    debug: false,
		    password: settings.password,
		    username: settings.nick
		});

		ircContainer.bot.connect(function() {
		    console.log("Connected!");
		});

		ircContainer.bot.addListener('message', function (from, to, message) {
		    try {
		        var parsed = parser.Parse(message);
		        if (parsed) 
		        	ircContainer.messagePool.push({
			        	from: from,
			        	to: to,
			        	type: parsed[0],
			        	args: parsed[1]
			        });
		    }
		    catch (err){
		        console.log("Error sending message: " + err);
		    }
		});

		ircContainer.bot.addListener('error', function(message) {

		    console.log('Error!' + message);
		    
		    ircContainer.bot.connect(function() {
		        console.log("Reconnected!");
		    });

		});

	}
};

module.exports = ircContainer;