var mouse = require('./mouse');

module.exports = function(user) { 

	return function (from, to){

		var leftOffset = (from - 1) * 31;
		var rightOffset = ((to || from) - 1) * 31;
		if (to) mouse.Click(1780, 600 + leftOffset);
		mouse.Click(1832, 600 + rightOffset);

		if (to)
			console.log(user + '=> Acting on ' + from + ' to ' + to);
		else
			console.log(user + '=> Acting on ' + from);
		
	}

};