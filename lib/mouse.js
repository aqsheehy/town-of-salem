var edge = require("edge");

var raw_click = edge.func(
{
    source: function() {/*

        using System.Threading.Tasks;
        using System.Runtime.InteropServices;

        public static class Keyboard {
    
			[System.Runtime.InteropServices.DllImport("user32.dll")]
			public static extern bool SetCursorPos(int x, int y);

			[System.Runtime.InteropServices.DllImport("user32.dll")]
			public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        }

        public class Startup {

            public async Task<object> Invoke(dynamic input){
            	int x = input.x;
            	int y = input.y;
                Keyboard.SetCursorPos(x, y);
			    Keyboard.mouse_event(0x02, x, y, 0, 0);
			    Keyboard.mouse_event(0x04, x, y, 0, 0);
                return true;
            }
        }

  */},
    references: ["System.Linq.dll"]
});

module.exports = {
	Click: function(x, y){
		raw_click({ x: x, y: y });
	}
};