var mouse = require('./mouse');
var keyboard = require('./keyboard');

module.exports = function(user){
	return function (text){
		mouse.Click(40, 1037);
		keyboard.Type(text);
		keyboard.HitEnter();
		console.log(user + " => Saying '" + text + "'");
	};
};