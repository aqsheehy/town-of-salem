var edge = require("edge");

var raw_type = edge.func(
{
    source: function() {/*

        using System.Threading.Tasks;
        using System.Runtime.InteropServices;

        public static class Keyboard {
    
            [DllImport("user32.dll")]
            public static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        }

        public class Startup {

            public static byte? CharacterKey(char c)
            {
                switch (c)
                {
                    case 'a':   return 0x41;
                    case 'b':   return 0x42;
                    case 'c':   return 0x43;
                    case 'd':   return 0x44;
                    case 'e':   return 0x45;
                    case 'f':   return 0x46;
                    case 'g':   return 0x47;
                    case 'h':   return 0x48;
                    case 'i':   return 0x49;
                    case 'j':   return 0x4A;
                    case 'k':   return 0x4B;
                    case 'l':   return 0x4C;
                    case 'm':   return 0x4D;
                    case 'n':   return 0x4E;
                    case 'o':   return 0x4F;
                    case 'p':   return 0x50;
                    case 'q':   return 0x51;
                    case 'r':   return 0x52;
                    case 's':   return 0x53;
                    case 't':   return 0x54;
                    case 'u':   return 0x55;
                    case 'v':   return 0x56;
                    case 'w':   return 0x57;
                    case 'x':   return 0x58;
                    case 'y':   return 0x59;
                    case 'z':   return 0x5A;
                    case ' ':   return 0x20;
                    case '0':   return 0x30;
                    case '1':   return 0x31;
                    case '2':   return 0x32;
                    case '3':   return 0x33;
                    case '4':   return 0x34;
                    case '5':   return 0x35;
                    case '6':   return 0x36;
                    case '7':   return 0x37;
                    case '8':   return 0x38;
                    case '9':   return 0x39;
                    case '/':   return 0xBF;
                    default:    return null;
                }
            }

            public void PressKey(byte key){
                Keyboard.keybd_event(key, 0, 0x0001, 0);
                Keyboard.keybd_event(key, 0, 0x0002, 0);
            }

            public void Write(string sentence){
                foreach (var c in sentence) {
                    var key = CharacterKey(char.ToLower(c));
                    if (key != null) PressKey(key.Value);
                }
            }

            public async Task<object> Invoke(dynamic input){
                string sentence = input.sentence as string;
                bool hitEnter = (bool)input.enter;
                if (sentence != "") Write(sentence);
                if (hitEnter) PressKey(0x0D);
                return true;
            }
        }

  */},
    references: ["System.Linq.dll"]
});

module.exports = {

    Type: function(text){
        raw_type({ sentence: text, enter: false });
    },

    HitEnter: function(){
        raw_type({ sentence: '', enter: true });
    },

    DropLine: function(){

    }

};