var mouse = require('./mouse');
var keyboard = require('./keyboard');

module.exports = function(user){

	return function (target, text){
		mouse.Click(40, 1037);
		keyboard.Type("/w " + target + " " + text);
		keyboard.HitEnter();
		console.log(user + ' => Whispering ' + target + " '" + text + "'");
	};

};