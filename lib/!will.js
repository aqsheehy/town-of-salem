var mouse = require('./mouse');
var keyboard = require('./keyboard');

module.exports = function(user){
	return function(type, text){

		if (text) text = text.substr(0, 32);

		// Click the icon
		mouse.Click(699, 27);

		// Click the bottom right
		mouse.Click(1148, 839);

		if (type == '+')
			keyboard.HitEnter();
		else (type == '-')
			keyboard.DropLine();

		if (text) 
			keyboard.Type(text);
		else if (type == '+')
			keyboard.HitEnter();

		// Click the icon
		mouse.Click(699, 27);

        if (type == '+')
			console.log(user + " => Adding '" + text + "' to will");
		else 
			console.log(user + " => Removing line from will");

	};
};