function Parse(text){

	if (!text || text[0] != '!') return null;

	var parts = text.split(' ');
	var name = parts[0].toLowerCase();

	function Identifier(index){
		var value = parts[index];
		if (!value) return null;
		var identifier = Number(value);
		if (identifier > 15) return null;
		return identifier;
	}

	function TextFrom(index) {
		if (!parts[index]) return null;
		return parts.slice(index).join(' ');
	}

	function PlusMinus(index) {
		var value = parts[index];
		if (!value) return null;
		if (value != "+" && value != "-") return null;
		return value;
	}

	switch (name){

		case '!say':
		case '/say':
			if (!TextFrom(1)) return null;
			return [ "!say", [ TextFrom(1) ] ];
		case '!whisper':
		case '/whisper':
			if (!Identifier(1) || !TextFrom(2)) return null;
			return [ "!whisper", [ Identifier(1) , TextFrom(2) ] ];
		case '!action':
		case '/action':
			if (!Identifier(1) || (parts[2] && !Identifier(2))) return null;
			return [ "!action", [ Identifier(1), Identifier(2) ] ];
		case '!will':
		case '/will':
			if (!PlusMinus(1)) return null;
			return [ "!will", [ PlusMinus(1), TextFrom(2) ] ];
		case '!vote': 
		case '/vote': 
			if (!Identifier(1)) return null;
			return [ "!vote", [ Identifier(1) ] ];
		case '!day':
		case '/day':
			if (parts[1] && !Identifier(1)) return null;
			return [ "!day", [ Identifier(1) ] ];
		case '!guilty':
		case '/guilty':
			return ['!guilty', []];
		case '!innocent':
		case '/innocent':
			return ['!innocent', []];

		case '!start':
		case '/start':
			return ['!start', [ TextFrom(1) ]];

		case '!shameless':
		case '/shameless':
			return ['!say', ['twitch.tv/twitchplayssalem']];

		default:
			return null;
	}
}


module.exports = { Parse: Parse };