var mouse = require('./mouse');

module.exports = function(user){

	return function(target){

		mouse.Click(750, 24);

		if (target)
		{	
			var offset = (target - 1) * 30;
			mouse.Click(1098, 326 + offset);
		}

		if (target)
			console.log(user + '=> Performing day action on ' + target);
		else 
			console.log(user + '=> Performing day action');

	};

};