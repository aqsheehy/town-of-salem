var expect = require('chai').expect;
var parser = require('../lib/parser').Parse;

/* 
	!say ""
	!whisper 1 Sup how's it going?
	!action 1 2
	!will + ""
	!will - ""
	!vote 1
	!day 1
	!agreed @23444
*/
describe("Parsing", function(){

	describe("Edge cases", function(){

		it("should handle empty strings", function(){
			var parsed = parser("");
			expect(parsed).to.equal(null);
		})

		it("should handle strings not starting with !", function(){
			var parsed = parser("hi");
			expect(parsed).to.equal(null);
		});

		it("should handle strings starting with ! but invalid type", function(){
			var parsed = parser("!blahblahblahblah");
			expect(parsed).to.equal(null);
		})

	});

	describe("!say", function(){

		it("should ignore empty !say", function(){
			var parsed = parser("!say");
			expect(parsed).to.equal(null);
		});

		it("should process !say", function(){
			var parsed = parser("!say This town is stupid");
			expect(parsed[0]).to.equal("!say");			
			expect(parsed[1][0]).to.equal("This town is stupid");
		});

	});

	describe("!whisper", function(){

		it("should ignore empty !whisper", function(){
			var parsed = parser("!whisper");
			expect(parsed).to.equal(null);
		});

		it("should ignore missing text", function(){
			var parsed = parser("!whisper 1");
			expect(parsed).to.equal(null);
		});

		it("should ignore invalid identifier", function(){
			var parsed = parser("!whisper This town is stupid");
			expect(parsed).to.equal(null);
		});

		it("should ignore out of range identifier", function(){
			var parsed = parser("!whisper 16 This town is stupid");
			expect(parsed).to.equal(null);
		});

		it("should process !whisper", function(){
			var parsed = parser("!whisper 1 This town is stupid");
			expect(parsed[0]).to.equal("!whisper");
			expect(parsed[1][0]).to.equal(1);
			expect(parsed[1][1]).to.equal("This town is stupid");
		});

	});

	describe("!action", function(){

		it("should ignore empty !action", function(){
			var parsed = parser("!action");
			expect(parsed).to.equal(null);
		});

		it("should ignore invalid identifiers", function(){
			var parsed = parser("!action blah blah blah");
			expect(parsed).to.equal(null);
		});

		it("should ignore invalid identifiers #2", function(){
			var parsed = parser("!action 1 blah blah");
			expect(parsed).to.equal(null);
		});

		it("should ignore out of ranger identifiers", function(){
			var parsed = parser("!action 16");
			expect(parsed).to.equal(null);
		});

		it("should ignore out of ranger identifiers #2", function(){
			var parsed = parser("!action 1 16");
			expect(parsed).to.equal(null);
		});

		it("should process !action", function(){
			var parsed = parser("!action 1");
			expect(parsed[0]).to.equal("!action");
			expect(parsed[1][0]).to.equal(1);
		});

		it("should process !action #2", function(){
			var parsed = parser("!action 1 2");
			expect(parsed[0]).to.equal("!action");
			expect(parsed[1][0]).to.equal(1);
			expect(parsed[1][1]).to.equal(2);
		});

	});

	describe("!will", function(){

		it("should ignore empty !will", function(){
			var parsed = parser("!will");
			expect(parsed).to.equal(null);
		});

		it("should ignore invalid assignment", function(){
			var parsed = parser("!will deerrrr ehqweoqhw ewqwef");
			expect(parsed).to.equal(null);
		});

		it("should process !will", function(){
			var parsed = parser("!will -");
			expect(parsed[0]).to.equal("!will");
			expect(parsed[1][0]).to.equal("-");
		});

		it("should process !will #2", function(){
			var parsed = parser("!will - Blah blah blah");
			expect(parsed[0]).to.equal("!will");
			expect(parsed[1][0]).to.equal("-");
			expect(parsed[1][1]).to.equal("Blah blah blah");
		});

		it("should process !will #3", function(){
			var parsed = parser("!will +");
			expect(parsed[0]).to.equal("!will");
			expect(parsed[1][0]).to.equal("+");
		});

		it("should process !will #4", function(){
			var parsed = parser("!will + Blah blah blah");
			expect(parsed[0]).to.equal("!will");
			expect(parsed[1][0]).to.equal("+");
			expect(parsed[1][1]).to.equal("Blah blah blah");
		});

	});

	describe("!vote", function(){

		it("should ignore empty !vote", function(){
			var parsed = parser("!vote");
			expect(parsed).to.equal(null);
		});

		it("should ignore out of range identifier", function(){
			var parsed = parser("!vote 16");
			expect(parsed).to.equal(null);
		});

		it("should ignore invalid identifier", function(){
			var parsed = parser("!vote blah blah blah");
			expect(parsed).to.equal(null);
		});

		it("should process !vote", function(){
			var parsed = parser("!vote 1");
			expect(parsed[0]).to.equal("!vote");
			expect(parsed[1][0]).to.equal(1);
		});

	});

	describe("!day", function(){

		it("should ignore out of range identifier", function(){
			var parsed = parser("!day 16");
			expect(parsed).to.equal(null);
		});

		it("should ignore invalid identifier", function(){
			var parsed = parser("!day blah blah blah");
			expect(parsed).to.equal(null);
		});

		it("should process !day", function(){
			var parsed = parser("!day");
			expect(parsed[0]).to.equal("!day");
			expect(parsed[1][0]).to.equal(null);
		});

		it("should process !day #2", function(){
			var parsed = parser("!day 1");
			expect(parsed[0]).to.equal("!day");
			expect(parsed[1][0]).to.equal(1);
		});

	});

	describe('!guilty', function(){

		it("should process !guilty", function(){
			var parsed = parser("!guilty");
			expect(parsed[0]).to.equal('!guilty');
		});

	});

	describe('!innocent', function(){

		it("should process !innocent", function(){
			var parsed = parser("!innocent");
			expect(parsed[0]).to.equal('!innocent');
		});

	});

	describe('!start', function(){

		it("should process !start", function(){
			var parsed = parser("!start");
			expect(parsed[0]).to.equal("!start");
			expect(parsed[1][0]).to.equal(null);
		});

		it("should process !start #2", function(){
			var parsed = parser("!start Whisper blah");
			expect(parsed[0]).to.equal("!start");
			expect(parsed[1][0]).to.equal("Whisper blah");
		});

	});

});