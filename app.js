var mouse = require('./lib/mouse');
var keyboard = require('./lib/keyboard');
var irc = require('./irc');

function RunIrc(){
    setTimeout(function(){

        try {
            irc.start();
        }
        catch(err){
            console.log('ERROR!!!');
            RunIrc();
        }

    }, 1000)
}

function ProcessPool(){
    setTimeout(function(){

        try {

            var message = irc.messagePool[Math.floor(Math.random()*irc.messagePool.length)];
            irc.messagePool = [];
            if (message)
                require('./lib/' + message.type)(message.from, message.to).apply(this, message.args);
        }
        catch (err){
            console.log('Processing error: ' + err);
        }

        ProcessPool();

    }, 1000);
}

function KeepAlive(){
    setTimeout(function(){
        try {
            mouse.Click(949, 1049);
            KeepAlive();
        }
        catch (err){
            console.log('Failed to keep alive: ' + err);
        }

    }, 120000);
}

ProcessPool();
KeepAlive();
RunIrc();